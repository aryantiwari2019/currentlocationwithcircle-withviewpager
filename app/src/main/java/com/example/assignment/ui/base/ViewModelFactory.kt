package com.example.assignment.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.assignment.data.repository.HomeRespository
import com.example.assignment.ui.main.viewmodel.BannerViewModel

class ViewModelFactory(): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BannerViewModel::class.java)) {
            return BannerViewModel(HomeRespository()) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}