package com.example.assignment.ui.main.view


import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.os.Looper
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.assignment.R
import com.example.assignment.data.model.BannercardData
import com.example.assignment.ui.base.ViewModelFactory
import com.example.assignment.ui.main.adapter.SliderAdapter
import com.example.assignment.ui.main.viewmodel.BannerViewModel
import com.example.assignment.utils.PermissionUtils
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity(), OnMapReadyCallback {
    lateinit var fusedLocationProviderClient:FusedLocationProviderClient
    lateinit var locationRequest:LocationRequest
    var locationCallback:LocationCallback?=null
    lateinit var viewModel:BannerViewModel
    private val LOCATION_PERMISSION_REQUEST_CODE = 1
    var ImageList: ArrayList<BannercardData>? = null
    private var mMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewModel()
        getLocalData()
        initialiseMap()

    }


    fun initialiseMap(){
        val mapFragment = supportFragmentManager.findFragmentById(R.id.g_map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

    }


    private fun setupViewModel() {
        viewModel = ViewModelProvider(this, ViewModelFactory()).get(BannerViewModel::class.java)
    }


    private fun getLocalData(){
        ImageList= ArrayList()
        viewModel.getBannerData().observe(this, androidx.lifecycle.Observer {
            ImageList = it
            setupViewPager()
        })
    }


    fun setupViewPager(){
        viewPager!!.setAdapter(SliderAdapter(this, ImageList));
        indicator!!.setupWithViewPager(viewPager, true);
        setupContinuousSliding();

    }

    fun setupContinuousSliding(){
        GlobalScope.launch {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    runOnUiThread {
                        if (viewPager.getCurrentItem() < ImageList!!.size - 1) {
                            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                        } else {
                            viewPager.setCurrentItem(0);
                        }
                    }

                }
            }, 1, 3000)
        }

    }

    override fun onStart() {
        super.onStart()
        when {
            PermissionUtils.isAccessFineLocationGranted(this) -> {
                when {
                    PermissionUtils.isLocationEnabled(this) -> {
                        setUpLocationListener() }
                    else -> {
                        PermissionUtils.showGPSNotEnabledDialog(this) } } }
            else -> {
                PermissionUtils.requestAccessFineLocationPermission(
                    this,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if(this::fusedLocationProviderClient.isInitialized){
            fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(this) -> {
                            setUpLocationListener()
                        }
                        else -> {
                            PermissionUtils.showGPSNotEnabledDialog(this)
                        }
                    }
                } else {
                    Toast.makeText(
                        this,
                        getString(R.string.location_permission_not_granted),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }


    //Default LatLong till we dont get current lat long from fused location client
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0
        val Defaultlatlong = LatLng(28.535517, 77.391029)
        mMap!!.addMarker(
            MarkerOptions().position(Defaultlatlong).title("Noida").icon(
                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)
            )
        )
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(Defaultlatlong))
        mMap!!.getUiSettings().setZoomControlsEnabled(true);
        mMap!!.setMinZoomPreference(13.0f);
        addCircle(Defaultlatlong)
    }


    //fetch  current lat long from fused location client
    @SuppressLint("MissingPermission")
    private fun setUpLocationListener() {
         fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
         locationRequest = LocationRequest().setInterval(0).setFastestInterval(0)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
         locationCallback= object : LocationCallback() {
             override fun onLocationResult(locationResult: LocationResult) {
                 super.onLocationResult(locationResult)
                 for (location in locationResult.locations) {
                     mMap!!.clear()
                     val currentlatlong = LatLng(location.latitude, location.longitude)
                     AnimateCamera(currentlatlong)
                     addCircle(currentlatlong)


                 }
             }
         }
        fusedLocationProviderClient!!.requestLocationUpdates(
            locationRequest, locationCallback, Looper.myLooper()

        )
    }



    //add circle with in radius of 500 meter
    fun addCircle(latLng: LatLng){
        val circleOptions = CircleOptions()
        circleOptions.center(latLng)
        circleOptions.radius(500.0)
        circleOptions.fillColor(Color.BLUE)
        circleOptions.strokeColor(Color.GREEN)
        circleOptions.strokeWidth(4f)
        mMap!!.addCircle(circleOptions)
    }



    //animate marker
    fun AnimateCamera(latLng: LatLng){
        val googlePlex = CameraPosition.builder()
            .target(latLng)
            .zoom(14f)
            .bearing(0f)
            .tilt(10f)
            .build()

        mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 3000, null)
        mMap!!.addMarker(
            MarkerOptions().position(latLng).title("Your Location").icon(
                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)
            )
        )

    }


}
