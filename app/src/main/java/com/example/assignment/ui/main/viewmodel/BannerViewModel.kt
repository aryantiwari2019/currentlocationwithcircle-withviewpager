package com.example.assignment.ui.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.assignment.data.model.BannercardData
import com.example.assignment.data.repository.HomeRespository

class BannerViewModel(var repository:HomeRespository) :ViewModel(){
   fun getBannerData(): LiveData<ArrayList<BannercardData>> {
        return repository.getSliderData()
    }
}