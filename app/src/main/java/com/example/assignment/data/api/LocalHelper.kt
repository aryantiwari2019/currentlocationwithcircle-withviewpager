package com.example.assignment.data.api

class LocalHelper {
    companion object{
        val localJson:String="{\n" +
                "\n" +
                "    \"Bannercard_data\": [{\n" +
                "            \"background_image\": \"http://52.151.193.94:5257/MpartnerNewApi/CardImage/Homepage_banner_16_10_2019.jpg\",\n" +
                "            \"title\": \"\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"background_image\": \"http://52.151.193.94:5257/MpartnerNewApi/CardImage/Homepage_supersinger_banner_16_10_2019.jpg\",\n" +
                "            \"title\": \"\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"background_image\": \"http://52.151.193.94:5257/MpartnerNewApi/CardImage/HKVA-Combo-banner-min.jpg\",\n" +
                "            \"title\": \"\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"background_image\": \"http://52.151.193.94:5257/MpartnerNewApi/CardImage/INBT-Combo-banner-min.jpg\",\n" +
                "            \"title\": \"\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"background_image\": \"http://52.151.193.94:5257/MpartnerNewApi/CardImage/banner-for-Mpartner-01-min.jpg\",\n" +
                "            \"title\": \"\"\n" +
                "        }\n" +
                "]\n" +
                "}"
    }

}