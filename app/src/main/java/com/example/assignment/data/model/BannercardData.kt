package com.example.assignment.data.model

data class BannercardData(
    val background_image: String,
    val title: String
)