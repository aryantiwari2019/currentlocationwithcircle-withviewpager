package com.example.assignment.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.assignment.data.api.LocalHelper
import com.example.assignment.data.model.BannercardData
import com.example.assignment.data.model.SliderModel
import com.google.gson.Gson

class HomeRespository {
    fun getSliderData():LiveData<ArrayList<BannercardData>>{
        var bannerlist= MutableLiveData<ArrayList<BannercardData>>()
        var gson= Gson();
        var data: SliderModel =gson.fromJson(LocalHelper.localJson, SliderModel::class.java)
        bannerlist!!.value=data.Bannercard_data
        return  bannerlist;
    }
}