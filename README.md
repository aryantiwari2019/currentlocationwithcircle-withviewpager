# CurrentLocationWithCircle-WithViewPager

this project contains a viewapger image auto scrolling functionality, and user can slide it manually , apart from that it also contains the current location and a circle within the radius of 500 meter of user current location, it will show location of noida till permission not granted, once permission will be granted it will redirect to user current location.

#### The app has following packages:
1. **data**: It contains all the data accessing and manipulating components.
2. **ui**: View classes along with their corresponding ViewModel.
4. **utils**: Utility classes.


<table style="width:100%">
  <tr>
    <th><img src="screenshot/Screenshot_2021-02-18-14-16-00-982_com.example.assignment.jpg"></th>
    <th><img src="screenshot/Screenshot_2021-02-18-14-16-06-388_com.example.assignment.jpg"></th>
    <th><img src="screenshot/Screenshot_2021-02-18-14-16-12-503_com.example.assignment.jpg"></th>
    <th><img src="screenshot/Screenshot_2021-02-18-14-16-23-924_com.example.assignment.jpg"></th>
  
  </tr>
</table>
